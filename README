石を十個 / Ishi o jū-ko / 10 stones
============

Description
-----------

石を十個 (Ishi o jū-ko / 10 stones) is a game, where you are challenged by
clearing lines of 10 blocks. This is achieved by positioning falling blocks
in a matching position.

Requirements / Compatibility
----------------------------

Currently, the game is developed with GNU/Linux (Debian) as target platform.
Therefore, other platforms (like Microsoft Windows, Apple MacOS X) are not
officially supported right now but they may work.

Dependencies
------------

Ishi o jū-ko depends on the following libaries and software packages:

 - libgl
 - libglu
 - libglew
 - libsdl2 v2.0.5
 - libsdl2-gfx
 - libsdl2-image v2.0.1
 - libsdl2-mixer

The following software is required for build:

 - automake
 - autoconf
 - g++ or any other c++ compiler with support for c++11
 - povray
 - sed
 - nc

Build
-----

Ishi o jū-ko depends on automake and autoconf, wherefore all you need to build
is to execute the following steps:

	./configure
	make

Installation
------------

As Ishi o jū-ko depends on automake and autoconf, you can install the game
simply via

	sudo make install

but be warned, as there are better ways to install software (see below).

Packaging
---------

Instructions to create distribution specific installation packages will
follow soon.

Contribute
----------

Contributions are welcome by any means. If you want to optimize our code,
add more backgrounds or music, feel free to do so. Please try to mimic our
style and aesthetics, as we want to provide a monolithic feel in the game.
Don't forget to add yourself to the contributors list (see AUTHORS).

As our main code repository and therefore development is on gitlab, you can
fork the project to your own namespace. When you're done with your changes,
issue an merge request. We will check if the changes do match to the project
and merge them into the main repository.
