#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <inttypes.h>

#define NUMBER_OF_ALTERNATE_MAPPINGS 3
#define GAMECONTROLLER_DEADZONE 8192

class Config {
public:
	Config();
	
	struct {
		uint16_t viewportWidth, viewportHeight;
		uint32_t windowFlags;
		uint32_t renderFlags;
	} video;
	
	enum inputDevice {
		INPUTDEVICE_NULL,
		INPUTDEVICE_KEYBOARD_KEY,
		INPUTDEVICE_MOUSE_KEY,
		INPUTDEVICE_MOUSE_AXIS,
		INPUTDEVICE_GAMECONTROLLER_KEY,
		INPUTDEVICE_GAMECONTROLLER_AXIS
	};
	
	struct inputAction {
		enum inputDevice device;
		
		union {
			SDL_Keycode keyboardInputKey;
			
			uint8_t mouseAxisId;
			uint8_t mouseButtonId;
		
			struct {
				SDL_JoystickID gameControllerDeviceId;
				
				union {
					uint8_t gameControllerAxisId;
					uint8_t gameControllerButtonId;
				};
			};
		};
		
	};
	
	// Attention:
	// inputPlayer[0] = player 1
	// inputPlayer[1] = player 1 alternate configuration 1
	// inputPlayer[2] = player 1 alternate configuration 2
	// inputPlayer[3] = player 2
	// inputPlayer[4] = player 2 alternate configuration 1
	// inputPlayer[5] = player 2 alternate configuration 2
	// inputPlayer[6] = player 3
	// inputPlayer[7] = player 3 alternate configuration 1
	// inputPlayer[8] = player 3 alternate configuration 2
	// inputPlayer[9] = player 4
	// inputPlayer[10] = player 4 alternate configuration 1
	// inputPlayer[11] = player 4 alternate configuration 2
	struct {
		struct inputAction actionUp;
		struct inputAction actionDown;
		struct inputAction actionLeft;
		struct inputAction actionRight;
		
		struct inputAction actionA;
		struct inputAction actionB;
		struct inputAction actionX;
		struct inputAction actionY;
		
		struct inputAction actionL;
		struct inputAction actionR;
		
		struct inputAction actionBack;
		struct inputAction actionStart;
		
	} inputPlayer[4 * NUMBER_OF_ALTERNATE_MAPPINGS];
	
	void applyArguments(int argc, char *argv[]);
	
};

#endif