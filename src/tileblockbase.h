#ifndef __TILEBLOCKBASE_H__
#define __TILEBLOCKBASE_H__
#include <inttypes.h>

extern uint8_t tileBlockBase_0[];
extern uint8_t tileBlockBase_1[];
extern uint8_t tileBlockBase_2[];
extern uint8_t tileBlockBase_3[];
extern uint8_t tileBlockBase_4[];
extern uint8_t tileBlockBase_5[];
extern uint8_t tileBlockBase_6[];
extern uint8_t tileBlockBase_7[];
extern uint8_t tileBlockBase_8[];
extern uint8_t tileBlockBase_9[];
extern uint8_t tileBlockBase_10[];
extern uint8_t tileBlockBase_11[];
extern uint8_t tileBlockBase_12[];
extern uint8_t tileBlockBase_13[];
extern uint8_t tileBlockBase_14[];
extern uint8_t tileBlockBase_15[];
extern uint8_t tileBlockBase_16[];
extern uint8_t tileBlockBase_17[];
extern uint8_t tileBlockBase_18[];
extern uint8_t tileBlockBase_19[];
extern uint8_t tileBlockBase_20[];
extern uint8_t tileBlockBase_21[];
extern uint8_t tileBlockBase_22[];
extern uint8_t tileBlockBase_23[];

#endif