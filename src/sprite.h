#ifndef __SPRITE_H__
#define __SPRITE_H__

#include "gameobject.h"

class Sprite: public GameObject {
public:
	Sprite(Game* game, Screen* screen, uint16_t xSiz, uint16_t ySiz);
	virtual void render();
	
};

#endif