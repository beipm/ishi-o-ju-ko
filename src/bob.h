#ifndef __BOB_H__
#define __BOB_H__

#include "gameobject.h"

class Bob: public GameObject {
public:
	Bob(Game* game, Screen* screen, uint16_t xSiz, uint16_t ySiz);
	virtual void render();
	
};

#endif