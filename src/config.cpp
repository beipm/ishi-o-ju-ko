#include <SDL2/SDL.h>

#include "config.h"

Config::Config() :
video({640, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE})
{
	
	for (int i = 0; i < 4 * NUMBER_OF_ALTERNATE_MAPPINGS; i++) {
		
		inputPlayer[i].actionUp.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionDown.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionLeft.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionRight.device = INPUTDEVICE_NULL;
		
		inputPlayer[i].actionA.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionB.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionX.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionY.device = INPUTDEVICE_NULL;
		
		inputPlayer[i].actionL.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionR.device = INPUTDEVICE_NULL;
		
		inputPlayer[i].actionBack.device = INPUTDEVICE_NULL;
		inputPlayer[i].actionStart.device = INPUTDEVICE_NULL;
		
	}
	
	// Standard-Eingabe-Config:
	inputPlayer[0].actionUp.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionUp.keyboardInputKey = SDLK_UP;
	
	inputPlayer[0].actionDown.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionDown.keyboardInputKey = SDLK_DOWN;
	
	inputPlayer[0].actionLeft.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionLeft.keyboardInputKey = SDLK_LEFT;
	
	inputPlayer[0].actionRight.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionRight.keyboardInputKey = SDLK_RIGHT;
	
	inputPlayer[0].actionA.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionA.keyboardInputKey = SDLK_a;
	
	inputPlayer[0].actionB.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionB.keyboardInputKey = SDLK_b;
	
	inputPlayer[0].actionX.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionX.keyboardInputKey = SDLK_x;
	
	inputPlayer[0].actionY.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionY.keyboardInputKey = SDLK_y;
	
	inputPlayer[0].actionL.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionL.keyboardInputKey = SDLK_l;
	
	inputPlayer[0].actionR.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionR.keyboardInputKey = SDLK_r;
	
	inputPlayer[0].actionBack.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionBack.keyboardInputKey = SDLK_BACKSPACE;
	
	inputPlayer[0].actionStart.device = INPUTDEVICE_KEYBOARD_KEY;
	inputPlayer[0].actionStart.keyboardInputKey = SDLK_RETURN;
	
	
	// Standard Gamepad Config
	for (int i = 0; i < 4; i++) {
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionUp.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionUp.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionUp.gameControllerButtonId = SDL_CONTROLLER_BUTTON_DPAD_UP;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionDown.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionDown.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionDown.gameControllerButtonId = SDL_CONTROLLER_BUTTON_DPAD_DOWN;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionLeft.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionLeft.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionLeft.gameControllerButtonId = SDL_CONTROLLER_BUTTON_DPAD_LEFT;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionRight.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionRight.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionRight.gameControllerButtonId = SDL_CONTROLLER_BUTTON_DPAD_RIGHT;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionUp.device = INPUTDEVICE_GAMECONTROLLER_AXIS;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionUp.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionUp.gameControllerAxisId = 0;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionDown.device = INPUTDEVICE_GAMECONTROLLER_AXIS;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionDown.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionDown.gameControllerAxisId = 1;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionLeft.device = INPUTDEVICE_GAMECONTROLLER_AXIS;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionLeft.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionLeft.gameControllerAxisId = 2;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionRight.device = INPUTDEVICE_GAMECONTROLLER_AXIS;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionRight.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 2].actionRight.gameControllerAxisId = 3;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionA.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionA.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionA.gameControllerButtonId = SDL_CONTROLLER_BUTTON_A;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionB.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionB.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionB.gameControllerButtonId = SDL_CONTROLLER_BUTTON_B;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionX.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionX.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionX.gameControllerButtonId = SDL_CONTROLLER_BUTTON_X;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionY.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionY.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionY.gameControllerButtonId = SDL_CONTROLLER_BUTTON_Y;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionL.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionL.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionL.gameControllerButtonId = SDL_CONTROLLER_BUTTON_LEFTSHOULDER;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionR.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionR.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionR.gameControllerButtonId = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionBack.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionBack.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionBack.gameControllerButtonId = SDL_CONTROLLER_BUTTON_BACK;
		
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionStart.device = INPUTDEVICE_GAMECONTROLLER_KEY;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionStart.gameControllerDeviceId = i;
		inputPlayer[(i * NUMBER_OF_ALTERNATE_MAPPINGS) + 1].actionStart.gameControllerButtonId = SDL_CONTROLLER_BUTTON_START;
		
	}
	
}


void Config::applyArguments(int argc, char *argv[])
{
}