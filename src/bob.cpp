#include "bob.h"
#include "game.h"

Bob::Bob(Game* game, Screen* screen, uint16_t xSiz, uint16_t ySiz): GameObject(game, screen, xSiz, ySiz) {
}

void Bob::render() {
	
	dst.x = xPos;
	dst.y = yPos;
	dst.w = xSiz;
	dst.h = ySiz;
	
	SDL_RenderCopy(parent->ren, texture, NULL, &dst);
	
}