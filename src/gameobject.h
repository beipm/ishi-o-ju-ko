#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include <inttypes.h>

#include "tilepad.h"
#include "tileblockbase.h"

#include <SDL2/SDL.h>

class Game;
class Screen;

class GameObject {
public:
	GameObject(Game* game, Screen* screen, uint16_t xSiz, uint16_t ySiz);
	virtual void render() = 0;
	
	void clear();
	void updateTexture();
	void setTile2bpp(uint8_t x, uint8_t y, uint8_t tileDefinition[], uint8_t color1, uint8_t color2, uint8_t color3, uint8_t color4);
	
	uint16_t xPos, yPos;
	uint16_t xCen, yCen;
	uint16_t xSiz, ySiz;
	
protected:
	Game* parent;
	Screen* screen;
	
	SDL_Rect dst;
	
	SDL_Texture* texture;
	SDL_Surface* surface;
	
};

#endif