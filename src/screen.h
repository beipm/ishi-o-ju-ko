#ifndef __SCREEN_H__
#define __SCREEN_H__

class Game;

#include "gameobject.h"

#include "sprite.h"
#include "bob.h"

class Screen {
public:
	Screen(Game* parent);
	
	virtual void handle() = 0;
	virtual void render() = 0;
	
	virtual void onEnter();
	virtual void onPart();
	
	Game* parent;
	
};

#endif