#ifndef __SCREENINGAME_H__
#define __SCREENINGAME_H__

#include "screen.h"

class ScreenIngame: public Screen {
public:
	ScreenIngame(Game* parent);
	
	virtual void handle();
	virtual void render();
	
	Bob bob;
	
};

#endif