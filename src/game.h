#ifndef __GAME_H__
#define __GAME_H__

#include <iostream>
#include <string>
#include <list>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

#include "config.h"
#include "screen.h"
#include "inputstate.h"

class ScreenIngame;

class Game {
public:
	
	Game();
	int start();
	void main();
	void allocatePalette();
	void changeScreen(Screen *newScreen);
	
	SDL_Window *win;
	SDL_Renderer *ren;
	
	Config config;
	
	bool keepLoop;
	
	struct InputState inputState[4];
	struct InputState inputStateLastFrame[4];
	
	SDL_Palette *palette;
	
protected:
	void checkInputState(SDL_Event &e, struct Config::inputAction &inputAction, bool &state);
	
	Screen *currentScreen;
	
	ScreenIngame *screenIngame;
	
};

#endif