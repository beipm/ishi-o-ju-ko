#include "stdlib.h"
#include "game.h"

#include "screeningame.h"

#define WINDOW_TITLE "石を十個 / Ishi o jū-ko"

Game::Game(): 
	win(nullptr), ren(nullptr), config(),
	keepLoop(true), currentScreen(nullptr)
{
	
	for (int i = 0; i < 4; i++) {
		
		inputState[i] = {
			// Up, Down, Left, Right
			0, 0, 0, 0,
			
			// A, B, X, Y
			0, 0, 0, 0,
			
			// L, R
			0, 0,
			
			// Start
			0
		};
		
	}
	
}

int Game::start() {
	
	int ret = EXIT_SUCCESS;
	
	std::cout << "initializing sdl layers" << std::endl;
	
	// SDL initialisieren
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		
		std::cout << "creating output window" << std::endl;
		
		// Fenster erstellen
		if ((bool) (win = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, config.video.viewportWidth, config.video.viewportHeight, config.video.windowFlags)))
		{
			
			std::cout << "creating renderer" << std::endl;
			
			// Renderer erstellen
			if ((bool) (ren = SDL_CreateRenderer(win, -1, config.video.renderFlags)))
			{
				
				SDL_RenderSetLogicalSize(ren, 256, 224);
				SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
				SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
				
				SDL_GameControllerEventState(SDL_QUERY);
				
				main();
				
				SDL_DestroyRenderer(ren);
				
			}
			else
			{
				
				std::cerr << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
				ret = EXIT_FAILURE;
				
			}
			
			SDL_DestroyWindow(win);
			
		}
		else
		{
			
			std::cerr << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
			ret = EXIT_FAILURE;
			
		}
		
		SDL_Quit();
		
	}
	else
	{
		
		std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
		ret = EXIT_FAILURE;
		
	}
	
	return ret;
	
}

void Game::changeScreen(Screen* newScreen)
{
	
	if (currentScreen != nullptr) {
		std::cout << "leaving screen " << currentScreen << std::endl;
		currentScreen->onPart();
	}
	
	currentScreen = newScreen;
	currentScreen->onEnter();
	
}

void Game::allocatePalette() {
	
	std::cout << "allocating palette" << std::endl;
	
	palette = SDL_AllocPalette(91);
	palette->colors[ 0] = {0x00, 0x00, 0x00, 0x00};
	palette->colors[ 1] = {0x1a, 0x1a, 0x1a, 0xff};
	palette->colors[ 2] = {0x33, 0x33, 0x33, 0xff};
	palette->colors[ 3] = {0x66, 0x66, 0x66, 0xff};
	palette->colors[ 4] = {0x99, 0x99, 0x99, 0xff};
	palette->colors[ 5] = {0xcc, 0xcc, 0xcc, 0xff};
	palette->colors[ 6] = {0xe6, 0xe6, 0xe6, 0xff};
	palette->colors[ 7] = {0x33, 0x29, 0x29, 0xff};
	palette->colors[ 8] = {0x66, 0x3d, 0x3d, 0xff};
	palette->colors[ 9] = {0x99, 0x3d, 0x3d, 0xff};
	
	palette->colors[10] = {0xcc, 0x52, 0x52, 0xff};
	palette->colors[11] = {0xff, 0x99, 0x99, 0xff};
	palette->colors[12] = {0xff, 0xcc, 0xcc, 0xff};
	palette->colors[13] = {0x33, 0x2d, 0x29, 0xff};
	palette->colors[14] = {0x66, 0x4e, 0x3d, 0xff};
	palette->colors[15] = {0x99, 0x63, 0x3d, 0xff};
	palette->colors[16] = {0xff, 0xa6, 0x67, 0xff};
	palette->colors[17] = {0xff, 0xc4, 0x99, 0xff};
	palette->colors[18] = {0xff, 0xe1, 0xcc, 0xff};
	palette->colors[19] = {0x33, 0x31, 0x29, 0xff};
	
	palette->colors[20] = {0x66, 0x5f, 0x3d, 0xff};
	palette->colors[21] = {0x99, 0x8a, 0x3d, 0xff};
	palette->colors[22] = {0xff, 0xe6, 0x67, 0xff};
	palette->colors[23] = {0xff, 0xee, 0x99, 0xff};
	palette->colors[24] = {0xff, 0xf7, 0xcc, 0xff};
	palette->colors[25] = {0x31, 0x33, 0x29, 0xff};
	palette->colors[26] = {0x5c, 0x66, 0x3d, 0xff};
	palette->colors[27] = {0x82, 0x99, 0x3d, 0xff};
	palette->colors[28] = {0xae, 0xcc, 0x52, 0xff};
	palette->colors[29] = {0xe6, 0xff, 0x99, 0xff};
	
	palette->colors[30] = {0xf2, 0xff, 0xcc, 0xff};
	palette->colors[31] = {0x2c, 0x33, 0x29, 0xff};
	palette->colors[32] = {0x4b, 0x66, 0x3d, 0xff};
	palette->colors[33] = {0x5c, 0x99, 0x3d, 0xff};
	palette->colors[34] = {0x7a, 0xcc, 0x52, 0xff};
	palette->colors[35] = {0xbb, 0xff, 0x99, 0xff};
	palette->colors[36] = {0xdd, 0xff, 0xcc, 0xff};
	palette->colors[37] = {0x29, 0x33, 0x2a, 0xff};
	palette->colors[38] = {0x3d, 0x66, 0x40, 0xff};
	palette->colors[39] = {0x3d, 0x99, 0x45, 0xff};
	
	palette->colors[40] = {0x52, 0xcc, 0x5c, 0xff};
	palette->colors[41] = {0x99, 0xff, 0xa1, 0xff};
	palette->colors[42] = {0xcc, 0xff, 0xd0, 0xff};
	palette->colors[43] = {0x29, 0x33, 0x2e, 0xff};
	palette->colors[44] = {0x3d, 0x99, 0x6b, 0xff};
	palette->colors[45] = {0x3d, 0x99, 0x6b, 0xff};
	palette->colors[46] = {0x52, 0xcc, 0x8f, 0xff};
	palette->colors[47] = {0x99, 0xff, 0xcc, 0xff};
	palette->colors[48] = {0xcc, 0xff, 0xe6, 0xff};
	palette->colors[49] = {0x29, 0x33, 0x32, 0xff};
	
	palette->colors[50] = {0x3d, 0x66, 0x63, 0xff};
	palette->colors[51] = {0x3d, 0x99, 0x91, 0xff};
	palette->colors[52] = {0x52, 0xcc, 0xc2, 0xff};
	palette->colors[53] = {0x99, 0xff, 0xf6, 0xff};
	palette->colors[54] = {0xcc, 0xff, 0xfb, 0xff};
	palette->colors[55] = {0x29, 0x30, 0x33, 0xff};
	palette->colors[56] = {0x3d, 0x58, 0x66, 0xff};
	palette->colors[57] = {0x3d, 0x7a, 0x99, 0xff};
	palette->colors[58] = {0x52, 0xa3, 0xcc, 0xff};
	palette->colors[59] = {0x99, 0xdd, 0xff, 0xff};
	
	palette->colors[60] = {0xcc, 0xee, 0xff, 0xff};
	palette->colors[61] = {0x29, 0x2c, 0x33, 0xff};
	palette->colors[62] = {0x3d, 0x47, 0x66, 0xff};
	palette->colors[63] = {0x3d, 0x54, 0x99, 0xff};
	palette->colors[64] = {0x52, 0x71, 0xcc, 0xff};
	palette->colors[65] = {0x99, 0xb3, 0xff, 0xff};
	palette->colors[66] = {0xcc, 0xd9, 0xff, 0xff};
	palette->colors[67] = {0x2b, 0x29, 0x33, 0xff};
	palette->colors[68] = {0x44, 0x3d, 0x66, 0xff};
	palette->colors[69] = {0x4c, 0x3d, 0x99, 0xff};
	
	palette->colors[70] = {0x66, 0x52, 0xcc, 0xff};
	palette->colors[71] = {0xaa, 0x99, 0xff, 0xff};
	palette->colors[72] = {0xd4, 0xcc, 0xff, 0xff};
	palette->colors[73] = {0x2f, 0x29, 0x33, 0xff};
	palette->colors[74] = {0x55, 0x3d, 0x66, 0xff};
	palette->colors[75] = {0x73, 0x3d, 0x99, 0xff};
	palette->colors[76] = {0x99, 0x52, 0xcc, 0xff};
	palette->colors[77] = {0xd4, 0x99, 0xff, 0xff};
	palette->colors[78] = {0xea, 0xcc, 0xff, 0xff};
	palette->colors[79] = {0x33, 0x29, 0x33, 0xff};
	
	palette->colors[80] = {0x66, 0x3d, 0x66, 0xff};
	palette->colors[81] = {0x99, 0x3d, 0x99, 0xff};
	palette->colors[82] = {0xcc, 0x52, 0xcc, 0xff};
	palette->colors[83] = {0xff, 0x99, 0xff, 0xff};
	palette->colors[84] = {0xff, 0xcc, 0xff, 0xff};
	palette->colors[85] = {0x33, 0x29, 0x2f, 0xff};
	palette->colors[86] = {0x66, 0x3d, 0x55, 0xff};
	palette->colors[87] = {0x99, 0x3d, 0x73, 0xff};
	palette->colors[88] = {0xcc, 0x52, 0x99, 0xff};
	palette->colors[89] = {0xff, 0x99, 0xd4, 0xff};
	
	palette->colors[90] = {0xff, 0xcc, 0xea, 0xff};
	
	std::cout << "done" << std::endl;
	
}

void Game::main() {
	
	Uint32 tickFrameStart(0), tickFrameDone(0), tickFrameDuration(0), tickFps(0), dst(1000 / 60), fps(0);
	SDL_Event e;
	char title[250];

	std::cout << "entered main " << std::endl;
	
	sprintf(title, WINDOW_TITLE);
	SDL_SetWindowTitle(win, title);
	
	allocatePalette();
	
	screenIngame = new ScreenIngame(this);
	
	changeScreen(screenIngame);
	
	do {
		
		tickFrameStart = SDL_GetTicks();
		
		for (int i = 0; i < 4; i++) {
			inputStateLastFrame[i] = inputState[i];
		}
		
		SDL_JoystickUpdate();
		
		while (SDL_PollEvent(&e))
		{
			switch (e.type)
			{
				case SDL_QUIT:
					std::cout << "EV: SDL_QUIT" << std::endl;
					keepLoop = false;
					break;
					
				case SDL_APP_TERMINATING:
					std::cout << "EV: SDL_APP_TERMINATING" << std::endl;
					break;
					
				case SDL_APP_LOWMEMORY:
					std::cout << "EV: SDL_APP_LOWMEMORY" << std::endl;
					break;
					
				case SDL_APP_WILLENTERBACKGROUND:
					std::cout << "EV: SDL_APP_WILLENTERBACKGROUND" << std::endl;
					break;
					
				case SDL_APP_DIDENTERBACKGROUND:
					std::cout << "EV: SDL_APP_DIDENTERBACKGROUND" << std::endl;
					break;
					
				case SDL_APP_WILLENTERFOREGROUND:
					std::cout << "EV: SDL_APP_WILLENTERFOREGROUND" << std::endl;
					break;
					
				case SDL_APP_DIDENTERFOREGROUND:
					std::cout << "EV: SDL_APP_DIDENTERFOREGROUND" << std::endl;
					break;
					
				case SDL_WINDOWEVENT:
					//std::cout << "EV: SDL_WINDOWEVENT" << std::endl;
					break;
					
				case SDL_SYSWMEVENT:
					std::cout << "EV: SDL_SYSWMEVENT" << std::endl;
					break;
					
				case SDL_KEYDOWN:
					//std::cout << "EV: SDL_KEYDOWN " << e.key.keysym.scancode << "-" << e.key.keysym.mod << "-" << e.key.keysym.sym << std::endl;
					break;
					
				case SDL_KEYUP:
					//std::cout << "EV: SDL_KEYUP " << e.key.keysym.scancode << "-" << e.key.keysym.mod << "-" << e.key.keysym.sym << std::endl;
					break;
					
				case SDL_TEXTEDITING:
					std::cout << "EV: SDL_TEXTEDITING" << std::endl;
					break;
					
				case SDL_KEYMAPCHANGED:
					std::cout << "EV: SDL_KEYMAPCHANGED" << std::endl;
					break;
					
				case SDL_MOUSEMOTION:
					//std::cout << "EV: SDL_MOUSEMOTION " << e.motion.which << "-" << e.motion.xrel << "-" << e.motion.yrel << std::endl;
					break;
					
				case SDL_MOUSEBUTTONDOWN:
					//std::cout << "EV: SDL_MOUSEBUTTONDOWN " << e.button.which << "-" << e.button.state << std::endl;
					break;
					
				case SDL_MOUSEBUTTONUP:
					//std::cout << "EV: SDL_MOUSEBUTTONUP " << e.button.which << "-" << e.button.state << std::endl;
					break;
					
				case SDL_MOUSEWHEEL:
					//std::cout << "EV: SDL_MOUSEWHEEL " << e.wheel.which << "-" << e.wheel.direction << std::endl;
					break;
					
				case SDL_JOYAXISMOTION:
					//std::cout << "EV: SDL_JOYAXISMOTION " << e.jaxis.which << "-" << e.jaxis.value << std::endl;
					break;
					
				case SDL_JOYBALLMOTION:
					//std::cout << "EV: SDL_JOYBALLMOTION " << e.jball.which << "-" << e.jball.xrel << "-" << e.jball.yrel << std::endl;
					break;
					
				case SDL_JOYHATMOTION:
					//std::cout << "EV: SDL_JOYHATMOTION " << e.jhat.which << "-" << e.jhat.value << std::endl;
					break;
					
				case SDL_JOYBUTTONDOWN:
					//std::cout << "EV: SDL_JOYBUTTONDOWN " << e.jbutton.which << "-" << e.jbutton.button << std::endl;
					break;
					
				case SDL_JOYBUTTONUP:
					//std::cout << "EV: SDL_JOYBUTTONUP " << e.jbutton.which << "-" << e.jbutton.button << std::endl;
					break;
					
				case SDL_JOYDEVICEADDED:
					std::cout << "EV: SDL_JOYDEVICEADDED " << e.jdevice.which << std::endl;
					//SDL_JoystickOpen(e.jdevice.which);
					break;
					
				case SDL_JOYDEVICEREMOVED:
					std::cout << "EV: SDL_JOYDEVICEREMOVED " << e.jdevice.which << std::endl;
					break;
					
				case SDL_CONTROLLERAXISMOTION:
					//std::cout << "EV: SDL_CONTROLLERAXISMOTION " << (int) e.caxis.which << "-" << (int) e.caxis.axis << "-" << (int) e.caxis.value << std::endl;
					break;
					
				case SDL_CONTROLLERBUTTONDOWN:
					//std::cout << "EV: SDL_CONTROLLERBUTTONDOWN " << (int) e.cbutton.which << "-" << (int) e.cbutton.button << std::endl;
					break;
					
				case SDL_CONTROLLERBUTTONUP:
					//std::cout << "EV: SDL_CONTROLLERBUTTONUP " << (int) e.cbutton.which << "-" << (int) e.cbutton.button << std::endl;
					break;
					
				case SDL_CONTROLLERDEVICEADDED:
					std::cout << "EV: SDL_CONTROLLERDEVICEADDED " << (int) e.cdevice.which << std::endl;
					SDL_GameControllerOpen(e.cdevice.which);
					break;
					
				case SDL_CONTROLLERDEVICEREMOVED:
					std::cout << "EV: SDL_CONTROLLERDEVICEREMOVED " << (int) e.cdevice.which << std::endl;
					break;
					
				case SDL_CONTROLLERDEVICEREMAPPED:
					std::cout << "EV: SDL_CONTROLLERDEVICEREMAPPED " << (int) e.cdevice.which << std::endl;
					break;
					
				case SDL_FINGERDOWN:
					std::cout << "EV: SDL_FINGERDOWN" << std::endl;
					break;
					
				case SDL_FINGERUP:
					std::cout << "EV: SDL_FINGERUP" << std::endl;
					break;
					
				case SDL_FINGERMOTION:
					std::cout << "EV: SDL_FINGERMOTION" << std::endl;
					break;
					
				case SDL_DOLLARGESTURE:
					std::cout << "EV: SDL_DOLLARGESTURE" << std::endl;
					break;
					
				case SDL_DOLLARRECORD:
					std::cout << "EV: SDL_DOLLARRECORD" << std::endl;
					break;
					
				case SDL_MULTIGESTURE:
					std::cout << "EV: SDL_MULTIGESTURE" << std::endl;
					break;
					
				case SDL_CLIPBOARDUPDATE:
					std::cout << "EV: SDL_CLIPBOARDUPDATE" << std::endl;
					break;
					
				case SDL_DROPFILE:
					std::cout << "EV: SDL_DROPFILE" << std::endl;
					break;
					
				case SDL_DROPTEXT:
					std::cout << "EV: SDL_DROPTEXT" << std::endl;
					break;
					
				case SDL_DROPBEGIN:
					std::cout << "EV: SDL_DROPBEGIN" << std::endl;
					break;
					
				case SDL_DROPCOMPLETE:
					std::cout << "EV: SDL_DROPCOMPLETE" << std::endl;
					break;
					
				case SDL_AUDIODEVICEADDED:
					std::cout << "EV: SDL_AUDIODEVICEADDED" << std::endl;
					break;
					
				case SDL_AUDIODEVICEREMOVED:
					std::cout << "EV: SDL_AUDIODEVICEREMOVED" << std::endl;
					break;
					
				case SDL_RENDER_TARGETS_RESET:
					std::cout << "EV: SDL_RENDER_TARGETS_RESET" << std::endl;
					break;
					
				case SDL_RENDER_DEVICE_RESET:
					std::cout << "EV: SDL_RENDER_DEVICE_RESET" << std::endl;
					break;
					
				case SDL_USEREVENT:
					std::cout << "EV: SDL_USEREVENT" << std::endl;
					break;
				
			}
			
			for (int i = 0; i < 8; i++) {
				
				this->checkInputState(e, config.inputPlayer[i].actionUp, inputState[i / 2].up);
				this->checkInputState(e, config.inputPlayer[i].actionDown, inputState[i / 2].down);
				this->checkInputState(e, config.inputPlayer[i].actionLeft, inputState[i / 2].left);
				this->checkInputState(e, config.inputPlayer[i].actionRight, inputState[i / 2].right);
				
				this->checkInputState(e, config.inputPlayer[i].actionA, inputState[i / 2].buttonA);
				this->checkInputState(e, config.inputPlayer[i].actionB, inputState[i / 2].buttonB);
				this->checkInputState(e, config.inputPlayer[i].actionX, inputState[i / 2].buttonX);
				this->checkInputState(e, config.inputPlayer[i].actionY, inputState[i / 2].buttonY);
				
				this->checkInputState(e, config.inputPlayer[i].actionL, inputState[i / 2].buttonL);
				this->checkInputState(e, config.inputPlayer[i].actionR, inputState[i / 2].buttonR);
				
				this->checkInputState(e, config.inputPlayer[i].actionBack, inputState[i / 2].buttonBack);
				this->checkInputState(e, config.inputPlayer[i].actionStart, inputState[i / 2].buttonStart);
				
			}
			
		}
		
		currentScreen->handle();
		currentScreen->render();
		
		fps++;
		
		tickFrameDone = SDL_GetTicks();
		tickFrameDuration = tickFrameDone - tickFrameStart;
		
		if (tickFps < tickFrameDone)
		{
			
			sprintf(title, "%s - %d fps", WINDOW_TITLE, fps);
			SDL_SetWindowTitle(win, title);
			
			tickFps = tickFrameDone + 1000;
			fps = 0;
			
		}
		
		if (tickFrameDuration < dst)
		{
			SDL_Delay(dst - tickFrameDuration);
		}
		
	}
	while (keepLoop);
	
}

void Game::checkInputState(SDL_Event& e, Config::inputAction& inputAction, bool& state) {
	
	// Keyboard-Key
	if ((inputAction.device == Config::INPUTDEVICE_KEYBOARD_KEY) && ((e.type == SDL_KEYDOWN) || (e.type == SDL_KEYUP)) && (e.key.keysym.sym == inputAction.keyboardInputKey)) {
		
		state = (e.type == SDL_KEYDOWN);
		
	}
	// Mouse-Key
	else if ((inputAction.device == Config::INPUTDEVICE_MOUSE_KEY) && ((e.type == SDL_MOUSEBUTTONDOWN) || (e.type == SDL_MOUSEBUTTONUP)) && (inputAction.mouseButtonId == e.button.button)) {
		
		state = (e.type == SDL_MOUSEBUTTONDOWN);
		
	}
	// Mouse-Axis
	else if ((inputAction.device == Config::INPUTDEVICE_MOUSE_AXIS) && (e.type == SDL_MOUSEMOTION)) {
		
		if (inputAction.mouseAxisId == 0) {
			state = (e.motion.xrel < 0);
		}
		else if (inputAction.mouseAxisId == 1) {
			state = (e.motion.xrel > 0);
		}
		else if (inputAction.mouseAxisId == 2) {
			state = (e.motion.yrel < 0);
		}
		else if (inputAction.mouseAxisId == 3) {
			state = (e.motion.xrel > 0);
		}
		
	}
	// Game Controller Key
	else if ((inputAction.device == Config::INPUTDEVICE_GAMECONTROLLER_KEY) && ((e.type == SDL_CONTROLLERBUTTONDOWN) || (e.type == SDL_CONTROLLERBUTTONUP)) && (e.cbutton.which == inputAction.gameControllerDeviceId) && (e.cbutton.button == inputAction.gameControllerButtonId)) {
		
		state = (e.type == SDL_CONTROLLERBUTTONDOWN);
		
	}
	// Game Controller Axis
	else if ((inputAction.device == Config::INPUTDEVICE_GAMECONTROLLER_AXIS) && (e.type == SDL_CONTROLLERAXISMOTION) && (e.cbutton.which == inputAction.gameControllerDeviceId) && (e.caxis.axis == inputAction.gameControllerAxisId / 2)) {
		
		if ((inputAction.gameControllerAxisId % 2) == 0) {
			state = e.caxis.value < -GAMECONTROLLER_DEADZONE;
		}
		else {
			state = e.caxis.value > GAMECONTROLLER_DEADZONE;
		}
		
	}
	
}


int main(int argc, char *argv[]) {
	
	Game game;
	game.config.applyArguments(argc, argv);
	return game.start();
	
}
