#ifndef __INPUTSTATE_H__
#define __INPUTSTATE_H__

struct InputState {
	
	bool up;
	bool down;
	bool left;
	bool right;
	
	bool buttonA;
	bool buttonB;
	bool buttonX;
	bool buttonY;
	
	bool buttonL;
	bool buttonR;
	
	bool buttonBack;
	bool buttonStart;
};

#endif