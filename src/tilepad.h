#ifndef __TILEPAD_H__
#define __TILEPAD_H__
#include <inttypes.h>

extern uint8_t tilePad_0[];
extern uint8_t tilePad_1[];
extern uint8_t tilePad_2[];
extern uint8_t tilePad_3[];
extern uint8_t tilePad_4[];
extern uint8_t tilePad_5[];
extern uint8_t tilePad_6[];
extern uint8_t tilePad_7[];
extern uint8_t tilePad_8[];
extern uint8_t tilePad_9[];
extern uint8_t tilePad_10[];
extern uint8_t tilePad_11[];

#endif