#include "screeningame.h"
#include "game.h"

#include "tileblockbase.h"
#include "tilepad.h"

ScreenIngame::ScreenIngame(Game* parent): Screen(parent), bob(parent, this, 128, 128) {
	
	bob.setTile2bpp(0, 0, tileBlockBase_0, 1, 2, 3, 4);
	bob.updateTexture();
	
}

void ScreenIngame::handle() {
}

void ScreenIngame::render() {
	
	SDL_RenderClear(parent->ren);
	
	bob.render();
	
	SDL_RenderPresent(parent->ren);
	
}