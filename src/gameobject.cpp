#include "gameobject.h"
#include "game.h"
#include "screen.h"

GameObject::GameObject(Game* game, Screen* screen, uint16_t xSiz, uint16_t ySiz): xPos(0), yPos(0), xCen(xSiz / 2), yCen(ySiz / 2), xSiz(xSiz), ySiz(ySiz), parent(game), screen(screen) {
	
	surface = SDL_CreateRGBSurface(0, xSiz, ySiz, 8, 0, 0, 0, 0);
	
	if (surface == NULL) {
		fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
		exit(1);
	}
	
	SDL_SetSurfacePalette(surface, parent->palette);
	
	clear();
	
	texture = SDL_CreateTextureFromSurface(parent->ren, surface);
	
	if (texture == NULL) {
		fprintf(stderr, "SDL_CreateTextureFromSurface failed: %s\n", SDL_GetError());
		exit(1);
	}
	
}

void GameObject::clear() {
	
	SDL_Rect rect;
	
	rect.x = 0;
	rect.y = 0;
	rect.w = xSiz;
	rect.h = ySiz;
	
	SDL_FillRect(surface, &rect, 3);
	
}

void GameObject::updateTexture() {
	
	SDL_DestroyTexture(texture);
	texture = SDL_CreateTextureFromSurface(parent->ren, surface);
	
	if (texture == NULL) {
		fprintf(stderr, "SDL_CreateTextureFromSurface failed: %s\n", SDL_GetError());
		exit(1);
	}
	
}

void GameObject::setTile2bpp(uint8_t x, uint8_t y, uint8_t tileDefinition[], uint8_t color1, uint8_t color2, uint8_t color3, uint8_t color4) {
	
	int bx = x * 8;
	int by = y * 8;
	
	
	
	for (int py = 0; py < 8; py++) {
		for (int px = 0; px < 8; px++) {
			
			//uint8_t *target_pixel = (uint8_t *) surface->pixels + (by + py) * surface->pitch + (bx + px) * sizeof *target_pixel;
			//*target_pixel = (px + py) % 255;
			
		}
	}
	
}